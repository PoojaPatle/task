package com.cloudage.assignment.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.cloudage.assignment.R;
import com.cloudage.assignment.adapter.PizzaItemAdapter;
import com.cloudage.assignment.model.Pizza;
import com.cloudage.assignment.utils.MyAdapterListener;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MyAdapterListener {

    ArrayList<Pizza> mPizzas = new ArrayList<>();

    RecyclerView recyclerView;

    PizzaItemAdapter pizzaItemAdapter;

    String fileName = "PizzaBill.txt";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUI();

        pizzaItemAdapter = new PizzaItemAdapter(getPizzaItems());
        pizzaItemAdapter.setAdapterListener(this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(pizzaItemAdapter);


    }

    private ArrayList<Pizza> getPizzaItems() {
        mPizzas.clear();
        mPizzas.add(new Pizza("Cheese Pizza", 150));
        mPizzas.add(new Pizza("Corn Pizza", 150));
        mPizzas.add(new Pizza("Farmers choice", 250));
        mPizzas.add(new Pizza("Chicken Pizza", 350));
        mPizzas.add(new Pizza("exit", 0));
        return mPizzas;
    }

    private void initUI() {

        recyclerView = (RecyclerView) findViewById(R.id.itemLists);
    }

    @Override
    public void onItemClick(int position) {

        if (position == 4) {
            Toast.makeText(this, "Stay Safe Stay Happy", Toast.LENGTH_SHORT).show();
            new Handler().postDelayed(() -> {
                finish();
            }, 1000L);
        } else {

            // Create an alert builder
            AlertDialog.Builder builder
                    = new AlertDialog.Builder(this);
            builder.setTitle("Quantity");

            // set the custom layout
            final View customLayout
                    = getLayoutInflater()
                    .inflate(
                            R.layout.custom_layout,
                            null);
            builder.setView(customLayout);

            // send data from the
            // AlertDialog to the Activity
            EditText editText = customLayout.findViewById(R.id.editTextQuantity);


            // add a button
            builder.setPositiveButton(
                    "OK",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(
                                DialogInterface dialog,
                                int which) {
                            Pizza item = mPizzas.get(position);

                            String quan = editText.getText().toString();

                            if (TextUtils.isEmpty(quan)) {
                                Toast.makeText(MainActivity.this, "Provide quantity", Toast.LENGTH_SHORT).show();
                            } else {
                                sendDialogDataToActivity(item, quan);
                            }

                        }
                    });

            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            // create and show
            // the alert dialog
            AlertDialog dialog
                    = builder.create();
            dialog.show();


        }

    }

    // coming from the AlertDialog
    private void sendDialogDataToActivity(Pizza pizza, String quantity) {
        // Declare variables

        double actualPrice = pizza.getPrice(), salesTax = 0;  // inputs and results
        double totalPrice = 0.0, totalActualPrice = 0.0, totalSalesTax = 0.0;  // to accumulate


        salesTax = (actualPrice * 10) / 100;

        totalSalesTax = salesTax * Double.parseDouble(quantity);

        totalPrice = actualPrice * Double.parseDouble(quantity);
        totalActualPrice = totalPrice + totalSalesTax;


        if (isExternalStorageAvailable() || isExternalStorageReadable()) {

            // Writing to a file using BufferedWriter in Java
            try {
                FileWriter writer = new FileWriter(getStorageDir(fileName));
                BufferedWriter bwr = new BufferedWriter(writer);
                bwr.write("Pizza Order Bill");
                bwr.write("\n");
                // Write in file
                bwr.newLine();
                bwr.write("Quantity = " + quantity);
                bwr.newLine();
                bwr.write("Tax = Rs." + totalSalesTax);
                bwr.newLine();
                bwr.write("amount = Rs." + actualPrice);
                bwr.newLine();
                bwr.write("Total Paid Amount = " + totalActualPrice);
                bwr.newLine();
                bwr.close();
                Log.i("file", "successfully written to a file");
                String message = "Total Actual Price is:" + String.format("%.2f", totalActualPrice) + "\n" +
                        "Total tax is:" + String.format("%.2f", totalSalesTax);
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }

        }

    }

    //checks if external storage is available for read and write
    public boolean isExternalStorageAvailable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    //checks if external storage is available for read
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public String getStorageDir(String fileName) {
        //create folder
        File file = new File(Environment.getExternalStorageDirectory(), "Pizza");
        if (!file.mkdirs()) {
            file.mkdirs();
        }
        String filePath = file.getAbsolutePath() + File.separator + fileName;
        return filePath;
    }

}
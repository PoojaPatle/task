package com.cloudage.assignment.ui;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.cloudage.assignment.R;

public class LoginActivity extends AppCompatActivity {

    EditText editTextName;
    Button actionLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initUI();


        actionLogin.setOnClickListener(v -> {

            String name = editTextName.getText().toString();

            if (TextUtils.isEmpty(name)) {
                Toast.makeText(this, "Please provide user name.", Toast.LENGTH_SHORT).show();
            } else {

                Toast.makeText(this, "Welcome " + name.toUpperCase(), Toast.LENGTH_SHORT).show();

                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                finishAffinity();
            }

        });

    }

    private void initUI() {
        editTextName = (EditText) findViewById(R.id.edit_username);
        actionLogin = (Button) findViewById(R.id.actionLogin);
    }
}
package com.cloudage.assignment.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.cloudage.assignment.R;
import com.cloudage.assignment.model.Pizza;
import com.cloudage.assignment.utils.MyAdapterListener;

import java.util.ArrayList;

public class PizzaItemAdapter extends RecyclerView.Adapter<PizzaItemAdapter.PizzaViewHolder> {

    ArrayList<Pizza> pizzas = new ArrayList<>();

    MyAdapterListener adapterListener;

    public PizzaItemAdapter(ArrayList<Pizza> pizzas) {
        this.pizzas = pizzas;
    }

    public void setAdapterListener(MyAdapterListener adapterListener) {
        this.adapterListener = adapterListener;
    }


    @NonNull
    @Override
    public PizzaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.pizza_item_lists, parent, false);
        return new PizzaViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(@NonNull PizzaViewHolder holder, int position) {

        Pizza pizza = pizzas.get(position);

        holder.pizza.setText(pizza.getName());
        if (pizza.getName().contentEquals("exit")) {
            holder.pizzaPrice.setVisibility(View.GONE);
        }
        if (pizza.getPrice() != 0) {
            holder.pizzaPrice.setText(String.valueOf(pizza.getPrice()));
        }

        holder.view.setOnClickListener(v -> adapterListener.onItemClick(position));
    }

    @Override
    public int getItemCount() {
        return pizzas.size();
    }

    public class PizzaViewHolder extends RecyclerView.ViewHolder {

        TextView pizza, pizzaPrice;
        View view;

        public PizzaViewHolder(@NonNull View itemView) {
            super(itemView);
            view = itemView;
            pizza = (TextView) view.findViewById(R.id.pizza_name);
            pizzaPrice = (TextView) view.findViewById(R.id.pizza_price);
        }
    }
}
